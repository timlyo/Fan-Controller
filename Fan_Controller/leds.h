#include <FastLED.h>
#include <Adafruit_NeoPixel.h>
#include "fans.h"

// Thoughts
// Colour = temp
// Rotation speed = fan speed
// Maybe brightness = fan speed

const int NUM_LEDS = 12;
const int DATA_PIN = D4;

float average_temp = 40;
float fan_speed = 1024;
int num_lit = 2;
int start = 0;
int noise = 0;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_LEDS, DATA_PIN, NEO_GRB + NEO_KHZ800);

void change_state();

void setup_leds() {
  pixels.begin();
  pixels.show();
}

// looped function for animation
void light() {
  change_state();

  // scale from 0 to 1
  float brightness = last_speed / 1024.0;
  brightness = 1.0;
    
  //base
  noise++;
  for (int x = 0; x < NUM_LEDS; x++) {
    uint16_t factor = noise + x * 10;
    uint8_t red = inoise8(factor) * 0.5;
    uint8_t green = inoise8(factor + 100) * 0.5;
    uint8_t blue = inoise8(factor + 200) * 0.5;
    pixels.setPixelColor(x, red, green, blue);

//    Serial.print("R:");
//    Serial.print(red);
//    Serial.print(" B:");
//    Serial.print(green);
//    Serial.print(" G:");
//    Serial.println(blue);
  }
  
  for (int x = 0; x < num_lit; x++) {
    uint32_t colour = pixels.Color(255, 255, 255);

    int index = (start + x) % NUM_LEDS;
    pixels.setPixelColor(index, colour);
  }

  if(wifi_connected() == true){
    pixels.setPixelColor(0, 255, 255, 0);
    pixels.setPixelColor(1, 255, 255, 0);
  }
  pixels.show();
}

// Increment state
void change_state() {
  static long count = 0;
  count++;

  if((count % 3) == 1){
    start = (start + 1) % NUM_LEDS;
  }
}

