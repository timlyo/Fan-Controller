#ifndef WIFI_H
#define WIFI_H

bool printed_ip = false;

bool wifi_connected(){
  return (WiFi.status() == WL_CONNECTED);
}

void connect_to_wifi(){
  // if connected
  if(WiFi.status() == WL_CONNECTED){
    // only print on first connection
    if(printed_ip == false){
      Serial.println("Connected");
      Serial.println(WiFi.localIP());
      printed_ip = true;
    }
    return;
  }

  // Otherwise connect
  Serial.println("Connecting to wifi");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
}

#endif // WIFI_H
