#include "config.h"
#include "wifi.h"

// Whether data has been sent to the database and the response has not yet been recived
bool sent_data = false;

void send_database_request(String request);
void connect_client();

/// Format a command into an http POST and send via send_database_request
void send_database_command(String command, String route){
  String request = String("POST " + route + " HTTP/1.1\r\n")+
                          "Host: "+ influxdb +":8086\r\n"+
                          "User-Agent: curl/7.52.1\r\n"+
                          "Accept: */*\r\n"+
                          "Content-Length: " + command.length() + "\r\n"+
                          "Content-Type: application/x-www-form-urlencoded\r\n"+
                          "\r\n"+
                          command;

  send_database_request(request);
}

void record_temp(float temp, String sensor){
  String command = String("temp,sensor=") + sensor + " value=";
  send_database_command(command + temp, "/write?db=mydb");
}

WiFiClient client;
unsigned long timeout;

void send_database_request(String request){
  if(sent_data){
    // Response from last post not recieved, don't send this one
    return;
  }

  if(wifi_connected() == false){
    return;
  }

  unsigned long start = millis();
  connect_client();

  Serial.print("POST:");
  Serial.println(request);
  client.print(request);
  Serial.println(String("Send took ") + (millis() - start) + "ms\n");
  sent_data = true;
  timeout = millis();
}

void connect_client(){
  if(wifi_connected() == false){
    Serial.println("No wifi");
    return;
  }
  
  if(client.connected()){
    return;
  }
    
  if(!client.connect(influxdb, 8086)) {
    Serial.println("Failed to connect to database");
  }
}

void recieve_database_response(){
  // if data, take all and print
  // if not measure timeout,
  //    if passed 5s cancel recieve
  //    if not return and try again later

  if(sent_data == false){
    return;
  }

  connect_client();
  
  if(client.available()){ // if data is ready
    unsigned long start = millis();
    
    Serial.print("RESPONSE:");
    while(client.available()){
      String line = client.readStringUntil('\r');
      Serial.print(line);
    }
    Serial.println(String("Recieve took ") + (millis() - start) + "ms\n");
    Serial.println(String("Send-recieve took ") + (millis() - timeout) + "ms\n");
    sent_data = false;
  } else if((millis() - timeout) > 5000) {
    Serial.println("Database send timed out");
    sent_data = false;
  }
}

