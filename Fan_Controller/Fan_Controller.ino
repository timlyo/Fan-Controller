#include <StaticThreadController.h>
#include <Thread.h>

#include <ESP8266WiFi.h>


#include "database.h";
#include "temperature.h";
#include "leds.h";
#include "wifi.h";
#include "fans.h";

void log_data();

// Handles the lighting of the leds
Thread led_thread               = Thread(light, 16);
// Handles sending data to the database
Thread database_send_thread     = Thread(log_data, 5000);
// Handles collecting and printing the response from the database
Thread database_response_thread = Thread(recieve_database_response, 500);
// Handles connecting to wifi and reconnecting upon errors
Thread wifi_connection_thread   = Thread(connect_to_wifi, 60000);
// Handles the speed control of the fans
Thread fan_controller           = Thread(set_fan_speed, 200);

StaticThreadController<5> controller (
    &led_thread,
    &fan_controller,
    &database_send_thread, 
    &database_response_thread, 
    &wifi_connection_thread
  );

void setup() {
  randomSeed(analogRead(0));
  Serial.begin(115200);
  Serial.println(LED_BUILTIN);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(12, OUTPUT);

  // Fans
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(10, OUTPUT);
  //pinMode(9, OUTPUT); // causes watchdog reset

  //Thermistors
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(D8, OUTPUT);

  
  // cuts out pwm noise
  analogWriteFreq(30 * 1000);
  
  setup_leds();
  
  Serial.println("Done setup");
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  //send_database_command("q=CREATE DATABASE mydb", "/query");
  controller.run();
}

// save fan speeds and temperature to database
void log_data(){
  print_temperatures();
  Serial.print("fan_speed=");
  Serial.println(last_speed);

  String command = String() + 
    "temp,sensor=intake value=" + get_temp(D5) +
    "\n temp,sensor=ambient value=" + get_temp(D6) +
    "\n temp,sensor=northbridge value=" + get_temp(D7) +
    "\n temp,sensor=CPU value=" + get_temp(D8) +
    "\n speed value=" + last_speed;

  send_database_command(command, "/write?db=comp");
}
