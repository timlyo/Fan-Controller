#ifndef FANS_H
#define FANS_H

unsigned int calculate_speed(float temp);
unsigned int last_speed = 0; // Read by the logging and led brightness functions only

unsigned int get_averaged_speed(float temp);

void set_fan_speed() {
  float average_temp = get_average_temperature();
  unsigned int speed = get_averaged_speed(average_temp);

  last_speed = speed;

  static int counter = 0;
  ++counter;

  analogWrite(D0, speed);
  analogWrite(D1, speed);
  analogWrite(D2, speed);
  analogWrite(D3, speed); // Top fans

  analogWrite(10, speed);
  //analogWrite(9, speed);
}

const int NUM_READINGS = 5;
unsigned int speeds[NUM_READINGS];
unsigned int ticker = 0;

unsigned int get_averaged_speed(float temp) {
  int new_speed = calculate_speed(temp);
  speeds[ticker] = new_speed;

  ticker = (ticker + 1) % NUM_READINGS;

  int total = 0;
  for (int x = 0; x < NUM_READINGS; x++) {
    total += speeds[x];
  }

  unsigned int speed = total / NUM_READINGS;
  Serial.print("Speed:");
  Serial.println(speed);
  return speed;
}

const int min_speed = 0;
const int max_speed = 1024;
const int min_temp = 30;
const int max_temp = 50;

unsigned int calculate_speed(float temp) {
  if (temp < min_temp) {
    return 0;
  } else if (temp > max_temp) {
    return 1024;
  }

  // Linear interpolation
  long result = (temp - min_temp) * ((max_speed - min_speed) / (max_temp - min_temp));

  if (result  > 1024) {
    return 1024;
  }
  return result;
}
#endif // FANS_H
