float steinhart(float resistance);

const int NUM_SAMPLES = 3;
const int ADC_PIN = A0;
const int DIODE_MODIFIER = (0.33/3.3) * 1023;

float get_temp(const int pin){
  digitalWrite(pin, HIGH);
  float total = 0;
  for(int i = 0; i < NUM_SAMPLES; i++){
    // accounts for diode in addition
    total += analogRead(ADC_PIN) + DIODE_MODIFIER;
    delay(10);
  }
  digitalWrite(pin, LOW);

  float average = total / NUM_SAMPLES;
  float reading = (1023.0 / average) -1;
  float temp = 10000 / reading;
  
  return steinhart(temp);
}

float steinhart(float resistance){
  const float NOMINAL_RESISTANCE = 10000; // of thermistor
  const float ROOM_TEMP = 21;
  const float BETA = 3435;

  float result = resistance / NOMINAL_RESISTANCE;
  result = log(result);
  result /= BETA;
  result += 1.0 / (ROOM_TEMP + 273.15);
  result = 1.0 / result;
  result -= 273.15;

  return result;
}

