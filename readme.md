This is the information for my fan controller

## Capabilities

* Up to 4 temperature probes
* Control up to 4 3 pin fans
* Control up to 2 4 pin fans
* Wifi connection (the IoT fridge was lonely)
* Sparkly lights

The project is based around the Nodemcu Esp8266 12E board
